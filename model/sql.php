<?php
    // получение данных без сортировки
    require_once 'functions.php';
    function getData($user_id) 
    {
        $sql = dbConnect()->prepare("SELECT u1.id AS user_id, task.id, description, is_done, date_added, u1.login AS login1, u2.login AS login2  FROM `task` LEFT JOIN `user` AS u1 ON u1.id=task.user_id LEFT JOIN `user` AS u2 ON u2.id=task.assigned_user_id WHERE user_id = :user_id");                
        $sql->execute([
            'user_id' => $user_id
        ]);
        return $sql;
    }
    
    // получение данных с сортировкой
    function getDataSort($selection, $user_id) 
    {
        $sql = dbConnect()->prepare("SELECT u1.id AS user_id, task.id, description, is_done, date_added, u1.login AS login1, u2.login AS login2  FROM `task` LEFT JOIN `user` AS u1 ON u1.id=task.user_id LEFT JOIN `user` AS u2 ON u2.id=task.assigned_user_id WHERE user_id = :user_id ORDER BY $selection");
        $sql->execute([
            ':user_id' => $user_id                    
        ]);
        return $sql;
    }

    // получение логинов пользователей для вывода в селект
    function getLogin() 
    {
        $sql_logins = dbConnect()->query("SELECT id, login  FROM `user`");
        $logins = $sql_logins->fetchAll();
        return $logins;
    }

    // получение списка задач, полученных от других пользователей
    function getRequire($user_id) 
    {
        $sql_require = dbConnect()->prepare("SELECT u2.id AS user_id, task.id, description, is_done, date_added, u1.login AS login1, u2.login AS login2  FROM `task` LEFT JOIN `user` AS u1 ON u1.id=task.user_id LEFT JOIN `user` AS u2 ON u2.id=task.assigned_user_id WHERE assigned_user_id = :user_id AND user_id != :user_id");            
        $sql_require->execute([
            'user_id' => $user_id
        ]);
        return $sql_require;
    }
    
    // удаление записи
    function deleteLine($id) 
    {
        $sql_delete = dbConnect()->prepare("DELETE FROM task WHERE `task`.`id` = :id");
        $sql_delete->bindValue(':id', $id, PDO::PARAM_INT);
        $sql_delete->execute();
        redirect('controller_admin');
    }

    // пометка записи, как выполненной
    function executeLine($id) 
    {
        $sql_done = dbConnect()->prepare("UPDATE `task` SET `is_done` = '1' WHERE `task`.`id` = :id");
        $sql_done->bindValue(':id', $id, PDO::PARAM_INT);
        $sql_done->execute();
        redirect('controller_admin');
    }

    // редактирование записи
    function editLine($id) 
    {
        $sql_edite = dbConnect()->prepare("SELECT * FROM task WHERE `task`.`id` = :id");
        $sql_edite->bindValue(':id', $id, PDO::PARAM_INT);
        $sql_edite->execute();
        $value = $sql_edite->fetch();
        return $value;
    }

    // вставка записи
    function createLine($user_id, $value, $assigned_user_id) 
    {
        $sql_insert = dbConnect()->prepare("INSERT INTO `task` (user_id, description, date_added, assigned_user_id) VALUES (:user_id, :value, NOW(), :assigned_user_id)");
        $sql_insert->execute([
            ':user_id' => $user_id,
            ':value' => $value,
            ':assigned_user_id' => $assigned_user_id
        ]);
        redirect('controller_admin');
    }

    // обновление отредактированной записи
    function updateLine($id, $description) 
    {
        $sql_update = dbConnect()->prepare("UPDATE `task` SET `description` = :description WHERE `task`.`id` = :id");        
        $sql_update->execute([
            ':id' => $id,
            ':description' => $description
        ]);        
        redirect('controller_admin');
    }

    // назначение нового пользователя ответственным за выполнение задачи
    function assignUser($user_id, $id) 
    {
        $sql_assign = dbConnect()->prepare("UPDATE `task` SET `assigned_user_id` = :user_id WHERE `task`.`id` = :id");                
        $sql_assign->execute([
            ':user_id' => $_GET['assign_user'],
            ':id' => $_GET['date_id']
        ]);            
        redirect('controller_admin'); 
    }    

    
    
