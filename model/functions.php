<?php
    session_start();    
    // Установка и проверка времени жизни сессии, если пользователь долго не обращался к странице - удаляем сессию и создаем новую
    function getTimeSession() 
    {
        $lifeLimit = 300;
        if(isset($_SESSION['time'])&&(time()-$_SESSION['time'])>$lifeLimit) {
                session_destroy();
                session_start();
                redirect('../index'); 
        }
        $_SESSION['time']=time();
    }      

    /**
     * коннектимся к базе данных
     * @return PDO object
     */
    function dbConnect() 
    {   
        $host = 'localhost';
        $dbname = 'homeworks';
        $user = 'root';
        $pass = '';      
        try {             
            $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $user, $pass);
        } catch (Exception $e) {
            die('Error: ' . $e->getMessage() . '<br/>');
        }
        return $db;
    }

    /**
     * получаем сведения о том, все ли необходимые данные для регистрации ввел пользователь
     * @return bool
     */
    function isData()
    {
        if ((isset($_POST['new_login']) || isset($_POST['new_password'])) && (empty($_POST['new_login']) || empty($_POST['new_password']))) {
            die("Введите все необходимые данные для регистрации");
            return false;
        } else {
            return true;
        }
    }

    /**
     * получаем данные логине вновь регистрируемого пользователя для проверки на существование
     * @return bool
     */
    function isUser()
    {   
        try {          
            $new_login = $_POST['new_login'];
            $sql = dbConnect()->prepare("SELECT `login` FROM `user` WHERE `login` LIKE ?");        
            $sql->execute([$new_login]);
            $user = [];
            $user = $sql->fetch();
            return !empty($user);
        } catch (Exception $e) {
            die('Error: ' . $e->getMessage() . '<br/>');
        }  
    } 

    /**
     * добавляем данные логине вновь регистрируемого пользователя
     * @return str
     */
    function addUser() 
    {
        try {          
            $newLogin = $_POST['new_login'];
            $newPass = md5($_POST['new_password']);
            $newUser = [
                ':newLogin' => $newLogin,
                ':newPass' => $newPass
            ];        
            $sql_add = "INSERT INTO `user` (`login`, `password`) VALUES (:newLogin, :newPass)";
            $addUser = dbConnect()->prepare($sql_add)->execute($newUser);
            if ($addUser == 1) {
                return "Вы успешно зарегистрированы. Введите логин и пароль, чтобы войти";
            } 
            if ($addUser == 0) {
                return "Ошибка при регистрации";
            }                  
        } catch (Exception $e) {
            die('Error: ' . $e->getMessage() . '<br/>');
        } 
    }

    /**
     * получаем данные о пользователе с введенным логином и паролем
     * @return bool
     */
    function isCorrectUser()
    {
        try { 
            $login = $_POST['login'];
            if (!empty($_POST['password'])) {
                $password = md5($_POST['password']);            
            } else {
                $password = null;
            }
            $sqlCorrectUser = dbConnect()->prepare("SELECT * FROM `user` WHERE `login` = :login AND `password` = :password");
            $sqlCorrectUser->execute([
                ':login' => $login,
                ':password' => $password
            ]);   
            $user = [];
            $user = $sqlCorrectUser->fetch();
            if (!empty($user)) {
                $_SESSION['user'] = $user;   
            }             
            return !empty($user);
        } catch (Exception $e) {
            die('Error: ' . $e->getMessage() . '<br/>');
        }  
    }     

    /**
     * Перенаправление на другую страницу
     * @param $page
     */
    function redirect($page) 
    {
        header("Location: $page.php");
        die;
    }
    /**
     * Проверка, является ли пользователь просто авторизованным
     * @return bool
     */
    function isAuthorized()
    {
        return !empty($_SESSION['user']);   
    }

?>