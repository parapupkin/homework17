<?php

/* login.html */
class __TwigTemplate_6f3c1c7f42e9e4c583ded9414dfa1f2bdc391a6be715927f6682c3640af3b86a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"ru\">
<head>
    <meta charset=\"UTF-8\"> 
    <link rel=\"stylesheet\" type=\"text/css\" href=\"./style.css\">  
    <title>Авторизация</title>
</head>
<body class=\"body_login\">
    <section id=\"login\">
        <div class=\"container\">                                            
            <h1>";
        // line 11
        if (array_key_exists("addUser", $context)) {
            echo twig_escape_filter($this->env, ($context["addUser"] ?? null), "html", null, true);
        }
        echo "</h1>
            <form class=\"form_login\" action=\"./index.php\" method=\"POST\">
                <div class=\"form-group\">
                    <label for=\"lg\" class=\"sr-only\">Логин</label> 
                    <input class=\"login_input\" type=\"text\" placeholder=\"Логин\" name=\"login\" id=\"lg\" class=\"form-control\">
                </div>
                <div class=\"form-group\">
                    <label for=\"key\" class=\"sr-only\">Пароль</label>
                    <input class=\"login_input\" type=\"password\"  placeholder=\"Пароль\" name=\"password\" id=\"key\" class=\"form-control\">
                </div>
                <input class=\"login_input\" type=\"submit\" class=\"btn\" value=\"Войти\">
            </form> 
            <h2>Не зарегистрированы? Не беда :) <br> Введите логин и пароль и нажмите зарегистрироваться</h2>     
            <form class=\"form_login\" action=\"./index.php\" method=\"POST\">
                <div class=\"form-group\">
                    <label for=\"new_lg\" class=\"sr-only\">Логин</label> 
                    <input class=\"login_input\" type=\"text\" placeholder=\"Имя\" name=\"new_login\" id=\"new_lg\" class=\"form-control\">
                </div> 
                <div class=\"form-group\">
                    <label for=\"new_key\" class=\"sr-only\">Пароль</label>
                    <input class=\"login_input\" type=\"password\"  placeholder=\"Пароль\" name=\"new_password\" id=\"new_key\" class=\"form-control\">
                </div>           
                <input class=\"login_input\" type=\"submit\" class=\"btn\" value=\"Зарегистрироваться\">
            </form>                    
        </div> <!-- /.container -->
    </section>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "login.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 11,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "login.html", "C:\\OSPanel\\domains\\homework17\\views\\login.html");
    }
}
