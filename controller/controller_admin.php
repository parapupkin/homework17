<?php
    //подключаем модель и автозагрузчик composer
    require_once '../model/functions.php';
    require_once '../model/sql.php';
    require_once '../vendor/autoload.php'; 
    //проверяем, как долго пользователь не обращался к странице, если более 5 минут - требуем заново авторизоваться
    getTimeSession();
    
    // если есть запросы по сортировке - выполняем запрос с сортировкой, если нет - выполняем запрос без сортировки

    if (!empty($_GET['selection'])) {
        switch ($_GET['selection']) {
            case 'date':
                $selection = 'task.date_added';
                break;
            case 'status':
                $selection = 'task.is_done';
                break;                             
            default:
                $selection = 'task.description';
                break;
        }                                                      
        $sql = getDataSort($selection, $_SESSION['user']['id']);       
    } else {
        $sql = getData($_SESSION['user']['id']);
    } 

    $data = $sql->fetchAll(PDO :: FETCH_ASSOC);        

    // запрос для получения списка пользователей (выводится в select)    
    $logins = getLogin();

    // запрос для вывода списка задач, полученных от других пользователей
    $sql_require = getRequire($_SESSION['user']['id']);
    $data_require = $sql_require->fetchAll(PDO :: FETCH_ASSOC);

    // если отправлен id дела, которое нужно отредактировать, записываем его
    if (!empty($_GET['id'])) {
        $id = $_GET['id'];       
    }  
    // по умолчанию поле для вставки новой записи предназначено для вставки, ниже есть возможность изменить его назначение при редактировании записей
    $name_insert = "insert";         
   
    if (isset($_GET['action'])) {
            // удаление записи
        if ($_GET['action'] == 'delete') {               
            deleteLine($id);            
        }

        // пометка записи, как выполненной
        if ($_GET['action'] == 'done') {                  
            executeLine($id);
        }

        // редактирование записи
        if ($_GET['action'] == 'edite') {           
            $value = editLine($id);
            $name_insert = "update";
        }       
    }

    // вставка записи
    if (!empty($_GET['insert'])) {
        createLine($_SESSION['user']['id'], $_GET['insert'], $_SESSION['user']['id']);        
    } 

    // обновление отредактированной записи
    if (!empty($_GET['update'])) {   
        updateLine($id, $_GET['update']);        
    }    

    // назначение нового пользователя ответственным за выполнение задачи
    if (isset($_GET['date_id']) && isset($_GET['assign_user'])) {
        assignUser($_GET['assign_user'], $_GET['date_id']);                         
    }    

    if ($_GET['logout'] == 1) {        
        session_destroy();
        redirect('../index');
    }    

    // Где лежат шаблоны
    @$loader = new Twig_Loader_Filesystem('../views');
    // Где будут хранится файлы кэша (php файлы)
    @$twig = new Twig_Environment($loader, array(
    'cache' => './tmp/cache',
    'auto_reload' => true,
    ));
    // Если была попытка добавления пользователя - выводим результат, если нет - слово "Авторизация"
    $params = ['user' => $_SESSION['user']['login'],
                'input_name' => $name_insert,
                'value' => $value['description'],
                'logins' => $logins,
                'data' => $data,
                'data_require' => $data_require,
                'id' => $id
    ];    
    
    echo $twig->render('admin.html', $params);
        
?>

   