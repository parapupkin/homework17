<?php
    require_once'model/functions.php';   

    // если авторизован - переходим на admin
    if (isAuthorized()) {
        redirect('controller/controller_admin');
    }
   
    // выполняем проверку, правильности введенных логина и пароля
    if (!empty($_POST['login'])) {        
        if (isCorrectUser()) {           
            redirect('controller/controller_admin');          
        } else {
            die("Неверно введен логин или пароль");
        }       
    }
    // выполняем проврку, существует ли такое имя пользователя при попытке регистрации 
    if (isData()) {
        if (!empty($_POST['new_login']) && !empty($_POST['new_password'])) { 
            // echo "ПРиввет";
            // exit();
            if (!isUser()) {
               $addUser = addUser();
            } else {
                die("Пользователь с таким именем уже существует");
            }    
        }        
    }  
    //Запускаем вывод на экран
    // Где лежат шаблоны
    $loader = new Twig_Loader_Filesystem('views');
    // Где будут хранится файлы кэша (php файлы)
    $twig = new Twig_Environment($loader, array(
    'cache' => './tmp/cache',
    'auto_reload' => true,
    ));
    // Если была попытка добавления пользователя - выводим результат, если нет - слово "Авторизация"
    if (isset($addUser)) {
        $params = ['addUser' => $addUser];
    } else {
        $params = ['addUser' => "Авторизация"];
    }
    
    echo $twig->render('login.html', $params);    
?>

